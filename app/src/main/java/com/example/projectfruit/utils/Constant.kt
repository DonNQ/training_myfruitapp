package com.example.projectfruit.utils

class Constant {

    enum class KeyEvent {
        UPDATE_FRUIT,
        DELETE_FRUIT,
    }

    companion object {
        const val DATABASE_NAME = "fruit_db"

        const val FRUIT_TABLE = "fruit_table"
        const val FRUIT_ID = "fruit_id"
        const val FRUIT_ID_CATEGORY = "fruit_id_category"
        const val FRUIT_NAME = "fruit_name"
        const val FRUIT_PRICE = "fruit_price"

        const val FRUIT_CATEGORIES_TABLE = "fruit_categories_table"
        const val FRUIT_CATEGORIES_ID = "fruit_categories_id"
        const val FRUIT_CATEGORIES_NAME = "fruit_categories_name"
        const val FRUIT_CATEGORIES_EXPANDED = "fruit_categories_expanded"
        const val REQUEST_CAMERA_PERMISSION = 1001

    }
}

