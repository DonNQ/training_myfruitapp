package com.example.projectfruit.utils

import android.app.Activity
import android.app.AlertDialog
import android.widget.Toast
import com.google.android.play.core.appupdate.AppUpdateInfo
import com.google.android.play.core.appupdate.AppUpdateManager
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.appupdate.testing.FakeAppUpdateManager
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.UpdateAvailability

class InAppUpdate(private val activity: Activity) {
    companion object {
        private const val REQUEST_CODE = 98
        private const val HIGH_PRIORITY = 5
        private const val NORMAL_PRIORITY = 4
        private const val UPDATE_VERSION = 5
    }


    private var appUpdateManager = AppUpdateManagerFactory.create(activity)


    init {
        appUpdateManager.appUpdateInfo.addOnSuccessListener { appUpdateInfo ->

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                if (appUpdateInfo.updatePriority() == HIGH_PRIORITY &&
                    appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.IMMEDIATE)
                ) {
                    startUpdate(
                        appUpdateInfo,
                        AppUpdateType.IMMEDIATE,
                        appUpdateInfo.availableVersionCode()
                    )
                } else if (appUpdateInfo.updatePriority() == NORMAL_PRIORITY &&
                    appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
                ) {
                    startUpdate(
                        appUpdateInfo,
                        AppUpdateType.FLEXIBLE,
                        appUpdateInfo.availableVersionCode()
                    )
                }
            }
        }
    }

    private fun startUpdate(info: AppUpdateInfo, type: Int, version: Int) {
        appUpdateManager.startUpdateFlowForResult(info, type, activity, REQUEST_CODE)
        AlertDialog.Builder(activity).apply {
            setTitle("Update available")
            setMessage("Update version $version available, process upgrade?")
            setPositiveButton("Yes") { _, _ ->
                Toast.makeText(
                    activity,
                    "The most recently version up to date",
                    Toast.LENGTH_SHORT
                )
                    .show()
            }
            setNegativeButton("No", null)
            show()
        }
    }
}
