package com.example.projectfruit.repositories

import com.example.projectfruit.data.local.dao.FruitDao
import com.example.projectfruit.data.model.Fruit
import com.example.projectfruit.data.model.FruitCategory
import com.example.projectfruit.data.remote.FirebaseRemote
import kotlinx.coroutines.CoroutineScope
import javax.inject.Inject

@Suppress("TooManyFunctions")
class FruitRepository @Inject constructor(
    private val fruitDao: FruitDao,
    private val firebaseRemote: FirebaseRemote
) {

    fun getFruitCategoryAndFruits() = fruitDao.getFruitCategoryAndFruits()

    suspend fun insertFruitCategory(data: FruitCategory) {
        fruitDao.insertFruitCategory(data)
    }

    suspend fun insertFruit(data: Fruit): Long {
        return fruitDao.insertFruit(data)
    }

    suspend fun updateFruitCategory(data: FruitCategory) {
        fruitDao.updateFruitCategory(data)
    }

    suspend fun deleteFruitCategory(data: FruitCategory) {
        fruitDao.deleteFruitCategory(data)
    }

    suspend fun deleteAllFruitOfCategory(id: Int) {
        fruitDao.deleteAllFruitOfCategory(id)
    }

    suspend fun deleteFruit(fruit: Fruit) {
        fruitDao.deleteFruit(fruit)
    }

    suspend fun updateFruit(id: Long?, name: String?, price: Int?) {
        fruitDao.updateFruit(id, name, price)
    }

    suspend fun deleteAllFruitCategory() = fruitDao.deleteAllFruitCategory()

    suspend fun deleteAllFruit() = fruitDao.deleteAllFruit()

    fun addNewFruitOnFirebase(title: String, fruit: Fruit) {
        firebaseRemote.addNewFruitOnFirebase(title, fruit)
    }

    fun deleteFruitFromFireBase(fruit: Fruit, fruitCategory: FruitCategory) {
        firebaseRemote.deleteFruitFromFireBase(fruit, fruitCategory)
    }

    fun deleteCategoryForFirebase(title: String) {
        firebaseRemote.deleteCategoryForFirebase(title)
    }

    fun addNewFruitCategoryForFireBase(title: String) {
        firebaseRemote.addNewCategory(title)
    }

    fun updateCategoryForFirebase(fromPathInput: String, toPathInput: String) {
        firebaseRemote.updateCategoryForFirebase(fromPathInput, toPathInput)
    }

    fun updateDataForFirebase(title: String, fruit: Fruit) {
        firebaseRemote.updateDataForFirebase(title, fruit)
    }

    suspend fun getDataFromFirebase(coroutineScope: CoroutineScope) {
        deleteAllFruit()
        deleteAllFruitCategory()
        firebaseRemote.getDataFromFirebase(fruitDao, coroutineScope)
    }

    fun getFruitById(id: Long): Fruit? {
        return fruitDao.getFruitById(id)
    }
}
