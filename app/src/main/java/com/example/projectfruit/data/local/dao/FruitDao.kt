package com.example.projectfruit.data.local.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import androidx.room.Update
import com.example.projectfruit.data.model.Fruit
import com.example.projectfruit.data.model.FruitCategory
import com.example.projectfruit.data.model.FruitCategoryAndFruits

@Suppress("TooManyFunctions", "WildcardImport")
@Dao
interface FruitDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFruitCategory(fruitCategory: FruitCategory?)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFruitCategories(fruitCategories: MutableList<FruitCategory>)

    @Update
    suspend fun updateFruitCategory(fruitCategory: FruitCategory?)

    @Delete
    suspend fun deleteFruitCategory(fruitCategory: FruitCategory?)

    @Query("SELECT * FROM FruitCategory")
    fun getAllFruitCategories(): LiveData<MutableList<FruitCategory>?>

    @Query("DELETE FROM FruitCategory WHERE id = :id")
    suspend fun deleteCFruitCategoryById(id: Int?)

    @Transaction
    @Query("SELECT * FROM FruitCategory")
    fun getFruitCategoryAndFruits(): LiveData<MutableList<FruitCategoryAndFruits>>

    // Fruit
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertFruit(fruit: Fruit?): Long

    @Delete
    suspend fun deleteFruit(fruit: Fruit?)

    @Query("UPDATE FRUIT SET name = :name, price= :price WHERE id =:id")
    suspend fun updateFruit(id: Long?, name: String?, price: Int?)

    @Query("SELECT * FROM FruitCategory WHERE id =:id")
    fun getCategoryNameById(id: Int): FruitCategory

    @Query("SELECT * FROM fruit ORDER BY id DESC LIMIT 1;")
    fun getTheLastFruitItem(): Fruit

    // Delete
    @Query("DELETE FROM FruitCategory")
    suspend fun deleteAllFruitCategory()

    @Query("SELECT * FROM Fruit WHERE id =:id")
    fun getFruitById(id: Long): Fruit?

    @Query("DELETE FROM Fruit")
    suspend fun deleteAllFruit()

    @Query("DELETE FROM FRUIT WHERE idFruitCategory = :id")
    suspend fun deleteAllFruitOfCategory(id: Int)
}
