package com.example.projectfruit.data.remote

import com.example.projectfruit.data.local.dao.FruitDao
import com.example.projectfruit.data.model.Fruit
import com.example.projectfruit.data.model.FruitCategory
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import javax.inject.Inject

@Suppress("WildcardImport")
class FirebaseRemote @Inject constructor() {
    companion object {
        const val FRUIT_LIST_TITLE = "fruit_list"
        const val CATEGORY_NAME_TITLE = "category_name"
    }

    private val refProduct: DatabaseReference by lazy {
        FirebaseDatabase.getInstance().reference.child("fruit")
    }


    fun addNewFruitOnFirebase(title: String, fruit: Fruit) {
        refProduct.child(title).child(FRUIT_LIST_TITLE).child(fruit.id.toString()).setValue(fruit)
    }

    fun addNewCategory(category: String) {
        refProduct.child(category).child(CATEGORY_NAME_TITLE).setValue(category)
    }

    fun deleteFruitFromFireBase(fruit: Fruit, fruitCategory: FruitCategory) {
        refProduct.child(fruitCategory.nameCategory!!).child(FRUIT_LIST_TITLE).child(fruit.id.toString()).removeValue()
    }

    fun deleteCategoryForFirebase(title: String) {
        refProduct.child(title).removeValue()
    }

    fun updateCategoryForFirebase(
        fromPathInput: String,
        toPathInput: String
    ) {
        val fromPath: DatabaseReference = refProduct.child(fromPathInput)
        val toPath: DatabaseReference = refProduct.child(toPathInput)
        val valueEventListener: ValueEventListener = object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                toPath.setValue(dataSnapshot.value).addOnCompleteListener { task ->
                    if (task.isComplete) {
                        refProduct.child(fromPathInput).removeValue()
                    }
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {
                return
            }
        }
        fromPath.addListenerForSingleValueEvent(valueEventListener)
    }

    fun updateDataForFirebase(title: String, fruit: Fruit) {
        val fruitHashMap: HashMap<String, String> = HashMap()
        val fruitHashMapInt = HashMap<String, Int>()
        fruitHashMap["name"] = fruit.name.toString()
        fruitHashMapInt["price"] = fruit.price!!.toInt()
        refProduct.child(title).child(FRUIT_LIST_TITLE).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for (data in snapshot.children) {
                    val currentFruit = data.getValue(Fruit::class.java)

                    /**
                     * VinhPX2
                     * update
                     * if (currentFruit?.idFruitCategory == fruit.idFruitCategory) {
                     */
                    // update code
                    if (currentFruit?.id == fruit.id) {     //end update

                        refProduct.child(title).child(data.key ?: "")
                            .updateChildren(fruitHashMap as Map<String, String>)
                            .addOnSuccessListener {
                            }.addOnFailureListener {

                            }
                        refProduct.child(title).child(data.key ?: "")
                            .updateChildren(fruitHashMapInt as Map<String, Int>)
                            .addOnSuccessListener {

                            }.addOnFailureListener {

                            }
                        return
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                return
            }

        })
    }

    fun getDataFromFirebase(fruitDao: FruitDao, coroutineScope: CoroutineScope) {
        refProduct.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                for ((count, data) in snapshot.children.withIndex()) {
                    val fruitCategory = FruitCategory()
                    fruitCategory.nameCategory = data.key
                    fruitCategory.id = count
                    refProduct.child(data.key ?: "").child(FRUIT_LIST_TITLE)
                        .addListenerForSingleValueEvent(object : ValueEventListener {
                            override fun onDataChange(snapshot: DataSnapshot) {
                                for (fruitData in snapshot.children) {
                                    val fruit = fruitData.getValue(Fruit::class.java)
                                    fruit?.idFruitCategory = count
                                    coroutineScope.launch {
                                        fruitDao.insertFruit(fruit)
                                    }
                                }
                            }

                            override fun onCancelled(error: DatabaseError) {
                                return
                            }
                        })
                    coroutineScope.launch {
                        fruitDao.insertFruitCategory(fruitCategory)
                    }
                }
            }

            override fun onCancelled(error: DatabaseError) {
                return
            }
        })
    }
}
