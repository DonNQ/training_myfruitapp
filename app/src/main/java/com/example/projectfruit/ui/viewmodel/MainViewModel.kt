package com.example.projectfruit.ui.viewmodel

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.projectfruit.data.model.Fruit
import com.example.projectfruit.data.model.FruitCategory
import com.example.projectfruit.data.model.FruitCategoryAndFruits
import com.example.projectfruit.repositories.FruitRepository
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.coroutines.launch


@Suppress("TooManyFunctions")
class MainViewModel @ViewModelInject constructor(
    private val fruitRepository: FruitRepository
) : ViewModel() {

    private val mCategory = fruitRepository.getFruitCategoryAndFruits()

    fun getMCategory(): LiveData<MutableList<FruitCategoryAndFruits>> {
        return mCategory
    }

    fun insertCategory(data: FruitCategory) = viewModelScope.launch {
        fruitRepository.insertFruitCategory(data)
    }

    fun addFruitToCategory(categoryName: String, fruit: Fruit) {
        viewModelScope.launch {
            val rowId = fruitRepository.insertFruit(fruit)
            getFruitById(rowId)?.let {
                fruitRepository.addNewFruitOnFirebase(categoryName, it)
            }
        }
    }

    fun updateCategory(data: FruitCategory) = viewModelScope.launch {
        fruitRepository.updateFruitCategory(data)
    }

    fun deleteCategory(data: FruitCategory) = viewModelScope.launch {
        fruitRepository.deleteFruitCategory(data)
    }

    fun deleteAllFruitOfCategory(id: Int) = viewModelScope.launch {
        fruitRepository.deleteAllFruitOfCategory(id)
    }

    fun getFruitById(id: Long) = fruitRepository.getFruitById(id)

    fun deleteFruit(fruit: Fruit, fruitCategory: FruitCategory) = viewModelScope.launch {
        fruitRepository.deleteFruit(fruit)
        fruitRepository.deleteFruitFromFireBase(fruit, fruitCategory)
    }

    fun updateFruit(id: Long?, name: String?, price: Int?) = viewModelScope.launch {
        fruitRepository.updateFruit(id, name, price)
    }

    fun deleteCategoryForFirebase(title: String) {
        fruitRepository.deleteCategoryForFirebase(title)
    }

    fun addNewCategory(category: String) {
        fruitRepository.addNewFruitCategoryForFireBase(category)
    }

    fun updateCategoryForFirebase(
        fromPathInput: String,
        toPathInput: String
    ) {
        fruitRepository.updateCategoryForFirebase(fromPathInput, toPathInput)
    }

    fun updateDataForFirebase(title: String, fruit: Fruit) {
        fruitRepository.updateDataForFirebase(title, fruit)
    }

    fun getDataFromFirebase() = viewModelScope.launch {
//        fruitRepository.deleteAllFruitCategory()
//        fruitRepository.deleteAllFruit()
        fruitRepository.getDataFromFirebase(viewModelScope)
    }
}
